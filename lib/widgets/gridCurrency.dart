import 'package:tasami_dashboard_app/screens/Currency_screen.dart';
import 'package:tasami_dashboard_app/screens/Vegetable_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class GridCurrency extends StatelessWidget {

  Items item1 = Items(id: 1, title: "عملات محلية", img: "assets/images/calendar.png");
  Items item2 = Items(id: 2, title: "خضروات", img: "assets/images/food.png");
  Items item3 = Items(id: 3, title: "مواد بناء");

  @override
  Widget build(BuildContext context) {
    List<Items> myList = [item1, item2, item3];
    var color = 0xff453658;
    return Flexible(
      child: GridView.count(
          childAspectRatio: 2.0,
          padding: EdgeInsets.only(left: 16, right: 16),
          crossAxisCount: 2,
          crossAxisSpacing: 18,
          mainAxisSpacing: 18,
          children: myList.map((data) {
            return InkWell(
              // onTap: () {
              //   switch (data.id) {
              //     case 1:
              //       Navigator.push(
              //           context,
              //           MaterialPageRoute(
              //               builder: (context) => CurrencyScreen()));
              //       break;
              //     case 2:
              //       Navigator.push(
              //           context,
              //           MaterialPageRoute(
              //               builder: (context) => VegetableScreen()));
              //       break;
              //     case 3:
              //       print(data.id);
              //       break;
              //   }
              // },
              child: Text(data.title),
            );
          }).toList()),
    );
  }
}

class Items {
  int id;
  String title;
  String img;

  Items({this.id, this.title, this.img});
}
