import 'package:flutter/material.dart';


// AppBar main_appBar({String title = "تسامي نيوز"}){
//   return AppBar(
// //    backgroundColor: Colors.white,
//     title:
//     Text(title, style: TextStyle(color: Colors.white , fontFamily: "Cairo") , maxLines: 1 , overflow: TextOverflow.ellipsis,),
//     actions: [
// //      IconButton(
// //          icon: Icon(
// //            Icons.view_headline,
// //          ), onPressed: (){}
// //      )
//     ],
//     flexibleSpace: Container(
//       decoration: BoxDecoration(
//         gradient: LinearGradient(
//             begin: Alignment.topLeft,
//             end: Alignment.bottomRight,
//             colors: <Color>[Color(0xFF141E30), Color(0xFF243B55)]),
//       ),
//     ),);
// }

TextStyle textStyle({double fontSize = 14, Color color = Colors.black, FontWeight fontWeight = FontWeight.normal}){
  return TextStyle(fontFamily: "Cairo", fontSize: fontSize , color: color , fontWeight: fontWeight);
}

const Color mainColor = Color(0xFF141E30);