import 'package:tasami_dashboard_app/data/model/Currency.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> setCurrency(Currency currency) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  double eur = (currency.eUR ?? prefs.getDouble('EUR'));
  double gbp = (currency.gBP ?? prefs.getDouble('GBP'));
  double tyr = (currency.tRY ?? prefs.getDouble('TRY'));
  double jod = (currency.jOD ?? prefs.getDouble('JOD'));
  double tnd = (currency.tND ?? prefs.getDouble('TND'));
  double egp = (currency.eGP ?? prefs.getDouble('EGP'));
  double tyrt = (currency.tRYTransfer ?? prefs.getDouble('TYRT'));
  double aedt = (currency.aEDTransfer ?? prefs.getDouble('AEDT'));
  double cheak = (currency.cheak ?? prefs.getDouble('CHK'));
  double fcard = (currency.familyCard ?? prefs.getDouble('FCD'));
  prefs.setDouble("EUR", eur);
  prefs.setDouble("GBP", gbp);
  prefs.setDouble("TYR", tyr);
  prefs.setDouble("JOD", jod);
  prefs.setDouble("TND", tnd);
  prefs.setDouble("EGP", egp);
  prefs.setDouble("TYRT", tyrt);
  prefs.setDouble("AEDT", aedt);
  prefs.setDouble("CHK", cheak);
  prefs.setDouble("FCD", fcard);
}

// getCurrencies() async {
//   SharedPreferences prefs = await SharedPreferences.getInstance();
//   int USD = (prefs.getInt('USD') ?? currency.uSD);
//   int EUR = (prefs.getInt('EUR') ?? currency.eUR);
//   int GBP = (prefs.getInt('USD') ?? currency.gBP);
//   int TYR = (prefs.getInt('USD') ?? currency.tRY);
//   int JOD = (prefs.getInt('USD') ?? currency.jOD);
//   int TND = (prefs.getInt('USD') ?? currency.tND);
//   int EGP = (prefs.getInt('USD') ?? currency.eGP);
//   int TYRT = (prefs.getInt('USD') ?? currency.tRYTransfer);
//   int AEDT = (prefs.getInt('USD') ?? currency.aEDTransfer);
//   return [];
// }
