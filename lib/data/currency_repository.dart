import 'package:http/http.dart' as http;

abstract class CurrencyRepository {
  Future<bool> addCurrency(List<double> price);
  Future<bool> addVegetables(List<double> price);
  Future<bool> addBuildingMaterials(List<double> price);
}

class ApiCurrencyRepository implements CurrencyRepository {
  @override
  Future<bool> addCurrency(List<double> price) async{
    try{
      Map<String, String> body = Map<String, String>();
      body = {
        "price[0]" : price[0].toString(),
        "item_id[0]" : 1.toString(),
        "price[1]" : price[1].toString(),
        "item_id[1]" : 2.toString(),
        "price[2]" : price[2].toString(),
        "item_id[2]" : 3.toString(),
        "price[3]" : price[3].toString(),
        "item_id[3]" : 4.toString(),
        "price[4]" : price[4].toString(),
        "item_id[4]" : 5.toString(),
        "price[5]" : price[5].toString(),
        "item_id[5]" : 6.toString(),
        "price[6]" : price[6].toString(),
        "item_id[6]" : 7.toString(),
        "price[7]" : price[7].toString(),
        "item_id[7]" : 8.toString(),
        "price[8]" : price[8].toString(),
        "item_id[8]" : 9.toString(),
        "price[9]" : price[9].toString(),
        "item_id[9]" : 10.toString(),
        "price[10]" : price[10].toString(),
        "item_id[10]" : 11.toString(),
        "price[11]" : price[11].toString(),
        "item_id[11]" : 12.toString(),
        "price[12]" : price[12].toString(),
        "item_id[12]" : 13.toString(),
      };
      var response = await http.post("https://new.tasaminews.ly/api/details",body: body);
      if (response.statusCode == 200) {
          return true;
      }else{
        return false;
      }
    }catch(_){
      print('Error from Response.\n');
      print(_);
    }
  }
  Future<bool> addVegetables(List<double> price) async{
    try{
      Map<String, String> body = Map<String, String>();
      body = {
        "price[0]" : price[0].toString(),
        "item_id[0]" : 1.toString(),
        "price[1]" : price[1].toString(),
        "item_id[1]" : 2.toString(),
        "price[2]" : price[2].toString(),
        "item_id[2]" : 3.toString(),
        "price[3]" : price[3].toString(),
        "item_id[3]" : 4.toString(),
        "price[4]" : price[4].toString(),
        "item_id[4]" : 5.toString(),
        "price[5]" : price[5].toString(),
        "item_id[5]" : 6.toString(),
        "price[6]" : price[6].toString(),
        "item_id[6]" : 7.toString(),
        "price[7]" : price[7].toString(),
        "item_id[7]" : 8.toString(),
        "price[8]" : price[8].toString(),
        "item_id[8]" : 9.toString(),
        "price[9]" : price[9].toString(),
        "item_id[9]" : 10.toString(),
        "price[10]" : price[10].toString(),
        "item_id[10]" : 11.toString(),
        "price[11]" : price[11].toString(),
        "item_id[11]" : 12.toString(),
        "price[12]" : price[12].toString(),
        "item_id[12]" : 13.toString(),
      };

      var response = await http.post("https://new.tasaminews.ly/api/details",body: body);
      if (response.statusCode == 200) {
        return true;
      }else{
        return false;
      }
    }catch(_){
      print('Error from Response.\n');
      print(_);
    }
  }
  Future<bool> addBuildingMaterials(List<double> price) async{
    try{
      Map<String, String> body = Map<String, String>();
      body = {
        "price[0]" : price[0].toString(),
        "item_id[0]" : 1.toString(),
        "price[1]" : price[1].toString(),
        "item_id[1]" : 2.toString(),
        "price[2]" : price[2].toString(),
        "item_id[2]" : 3.toString(),
        "price[3]" : price[3].toString(),
        "item_id[3]" : 4.toString(),
        "price[4]" : price[4].toString(),
        "item_id[4]" : 5.toString(),
        "price[5]" : price[5].toString(),
        "item_id[5]" : 6.toString(),
        "price[6]" : price[6].toString(),
        "item_id[6]" : 7.toString(),
        "price[7]" : price[7].toString(),
        "item_id[7]" : 8.toString(),
        "price[8]" : price[8].toString(),
        "item_id[8]" : 9.toString(),
        "price[9]" : price[9].toString(),
        "item_id[9]" : 10.toString(),
        "price[10]" : price[10].toString(),
        "item_id[10]" : 11.toString(),
        "price[11]" : price[11].toString(),
        "item_id[11]" : 12.toString(),
        "price[12]" : price[12].toString(),
        "item_id[12]" : 13.toString(),
      };

      var response = await http.post("https://new.tasaminews.ly/api/details",body: body);
      if (response.statusCode == 200) {
        return true;
      }else{
        return false;
      }
    }catch(_){
      print('Error from Response.\n');
      print(_);
    }
  }
}
