import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../store_static_currencies.dart';
import 'Currency.dart';

class Details {
  double usd;
  double gold;
  double silver;
  double eUR;
  double gBP;
  double tRY;
  // الدينار الاردني
  double jOD;
  double tND;
  double eGP;
  double cheak;
  double fcard;
  // الحوالة التركية
  double tRYTransfer;
  // حوالة دبي
  double aEDTransfer;

  Details({@required this.usd, @required this.gold, @required this.silver}) {
    this.calcPrice();
  }

  Future<void> calcPrice() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getDouble('EUR') == null) {
      await setCurrency(Currency(
        eUR: 1,
        gBP: 1,
        tRY: 1,
        jOD: 1,
        tND: 1,
        eGP: 1,
        cheak: 1,
        familyCard: 1,
        tRYTransfer: 1,
        aEDTransfer: 1,
      ));
    }

    this.eUR = this.usd / prefs.getDouble("EUR");
    this.gBP = this.usd / prefs.getDouble("GBP");
    this.tRY = this.usd / prefs.getDouble("TYR");
    this.jOD = this.usd / prefs.getDouble("JOD");
    this.tND = this.usd / prefs.getDouble("TND");
    this.eGP = this.usd / prefs.getDouble("EGP");
    this.cheak = this.usd + prefs.getDouble("CHK");
    this.fcard = this.usd + prefs.getDouble("FCD");
    this.tRYTransfer = this.usd + prefs.getDouble("TYRT");
    this.aEDTransfer = this.usd + prefs.getDouble("AEDT");
  }
}
