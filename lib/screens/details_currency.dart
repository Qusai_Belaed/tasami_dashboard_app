import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:tasami_dashboard_app/data/currency_repository.dart';
import 'package:tasami_dashboard_app/data/model/Currency.dart';
import 'package:tasami_dashboard_app/data/model/Details.dart';

import '../widgets/SharedWidget.dart';

class DetailsCurrency extends StatefulWidget {
  final Details details;
  DetailsCurrency({Key key, @required this.details}) : super(key: key);

  @override
  _DetailsCurrencyState createState() => _DetailsCurrencyState();
}

class _DetailsCurrencyState extends State<DetailsCurrency> {
  bool loaded = false;
  final _formKey = GlobalKey<FormState>();
  ApiCurrencyRepository repository;
  Currency currency;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    repository = ApiCurrencyRepository();
    currency = Currency();
    this.claculatePrice();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'التفاصيل',
          style: textStyle(color: Colors.white),
        ),
      ),
      body: (!loaded)
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextFormField(
                        onChanged: (value){
                          widget.details.usd= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.usd.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر الدولار'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الدولار',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.eUR= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.eUR.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر اليورو'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'اليورو',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.gBP= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.gBP.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر باوند'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'باوند',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.tND= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.tND.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر الدينار التونسي'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الدينار التونسي',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.eGP= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.eGP.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر الجنيه المصري'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الجنيه المصري',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.tRY= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.tRY.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر الليرة التركية'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الليرة التركية',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.jOD= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.jOD.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر الدينار الأردني'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الدينار الأردني',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.tRYTransfer= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.tRYTransfer.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر حوالة تركية'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'حوالة تركية',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.aEDTransfer= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.aEDTransfer.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر حوالة دبي'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'حوالة دبي',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.cheak = double.parse(value);
                        },
                        initialValue: widget.details.cheak.toStringAsFixed(2),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة الصك'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'شيك',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.fcard = double.parse(value);
                        },
                        initialValue: widget.details.fcard.toStringAsFixed(2),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة ارباب الاسر'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'ارباب الاسر',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.gold= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.gold.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر الذهب'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'سعر الذهب',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          widget.details.silver= double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: widget.details.silver.toStringAsFixed(2),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال سعر الفضة'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'سعر الفضة',
                        ),
                      ),
                      SizedBox(height: 12),
                    ],
                  ),
                ),
              ),
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          SweetAlert.show(
            context,
            title: "هل انت متاكد",
            style: SweetAlertStyle.confirm,
            showCancelButton: true,
            confirmButtonText: "تأكيد",
            cancelButtonText: "إالغاء",
            onPress: (bool isConfirm) {
              if (isConfirm) {
                // Todo: Save Data
                print('confirmed');
                List<double> price = List<double>();
                price.add(double.parse(widget.details.usd.toStringAsFixed(2)));
                price.add(double.parse(widget.details.eUR.toStringAsFixed(2)));
                price.add(double.parse(widget.details.gBP.toStringAsFixed(2)));
                price.add(double.parse(widget.details.tRY.toStringAsFixed(2)));
                price.add(double.parse(widget.details.jOD.toStringAsFixed(2)));
                price.add(double.parse(widget.details.tND.toStringAsFixed(2)));
                price.add(double.parse(widget.details.eGP.toStringAsFixed(2)));
                price.add(double.parse(widget.details.tRYTransfer.toStringAsFixed(2)));
                price.add(double.parse(widget.details.aEDTransfer.toStringAsFixed(2)));
                price.add(double.parse(widget.details.cheak.toStringAsFixed(2)));
                price.add(double.parse(widget.details.fcard.toStringAsFixed(2)));
                price.add(double.parse(widget.details.gold.toStringAsFixed(2)));
                price.add(double.parse(widget.details.silver.toStringAsFixed(2)));

                cheack(context , price);
              }
            },
          );
        },
        child: Icon(Icons.save),
      ),
    );
  }
  void cheack(context , price) async{
    bool TT = await repository.addCurrency(price);
    if(TT){
      SweetAlert.show(context,
          confirmButtonText: "تم",
          title: "تم الحفظ بنجاح",
          style: SweetAlertStyle.success);
    }else {
      SweetAlert.show(context,
          confirmButtonText: "تم",
          title: "فشل اتمام العملية",
          style: SweetAlertStyle.error);
    }
  }
  void claculatePrice() async {
    await widget.details.calcPrice();
    setState(() {
      loaded = true;
    });
  }
}
