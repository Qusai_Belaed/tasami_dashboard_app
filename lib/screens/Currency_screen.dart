import 'package:tasami_dashboard_app/data/model/Details.dart';
import 'package:tasami_dashboard_app/screens/static_currency.dart';
import 'package:tasami_dashboard_app/widgets/SharedWidget.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'details_currency.dart';

class CurrencyScreen extends StatefulWidget {
  @override
  _CurrencyScreenState createState() => _CurrencyScreenState();
}

class _CurrencyScreenState extends State<CurrencyScreen> {
  Details details = new Details(usd: 1, gold: 1, silver: 1);
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'العملات المحلية',
          style: textStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.info_outline),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => StaticCurrencyScreen()));
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
            key: _formKey,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    initialValue: this.details.usd.toString(),
                    onChanged: (value) {
                      this.details.usd = double.parse(value);
                    },
                    style: textStyle(fontSize: 16),
                    validator:
                        RequiredValidator(errorText: 'يرجاء ادخال الدولار'),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: mainColor)),
                      labelText: 'الدولار',
                    ),
                  ),
                  SizedBox(height: 12),
                  TextFormField(
                    initialValue: this.details.gold.toString(),
                    onChanged: (value) {
                      this.details.gold = double.parse(value);
                    },
                    style: textStyle(fontSize: 16),
                    validator:
                        RequiredValidator(errorText: 'يرجاء ادخال قيمة الذهب'),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: mainColor)),
                      labelText: 'الذهب',
                    ),
                  ),
                  SizedBox(height: 12),
                  TextFormField(
                    initialValue: this.details.silver.toString(),
                    onChanged: (value) {
                      this.details.silver = double.parse(value);
                    },
                    style: textStyle(fontSize: 16),
                    validator:
                        RequiredValidator(errorText: 'يرجاء ادخال قيمة الفضه'),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: mainColor)),
                      labelText: 'الفضه',
                    ),
                  ),
                ])),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: "التالي",
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      DetailsCurrency(details: this.details)));
        },
        child: Icon(Icons.arrow_forward_ios),
      ),
    );
  }
}
