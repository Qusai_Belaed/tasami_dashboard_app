import 'package:sweetalert/sweetalert.dart';
import 'package:tasami_dashboard_app/data/model/Currency.dart';
import 'package:tasami_dashboard_app/data/store_static_currencies.dart';
import 'package:tasami_dashboard_app/widgets/SharedWidget.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StaticCurrencyScreen extends StatefulWidget {
  @override
  _StaticCurrencyScreenState createState() => _StaticCurrencyScreenState();
}

class _StaticCurrencyScreenState extends State<StaticCurrencyScreen> {
  final _formKey = GlobalKey<FormState>();
  Currency currencies;
  bool loaded = false;

  @override
  void initState() {
    super.initState();
    getCurrencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('العملات الثابت', style: textStyle(color: Colors.white)),
      ),
      body: (!loaded)
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextFormField(
                        onChanged: (value) {
                          currencies.eUR = double.parse(value);
                        },
                        style: textStyle(fontSize: 16),
                        initialValue: currencies.eUR.toString(),
                        validator:
                            RequiredValidator(errorText: 'يرجاء ادخال اليورو'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'اليورو',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          currencies.gBP = double.parse(value);
                        },
                        initialValue: currencies.gBP.toString(),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة الباوند'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الباوند',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          currencies.tRY = double.parse(value);
                        },
                        initialValue: currencies.tRY.toString(),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة الليرة التركية'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الليرة التركية',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          currencies.jOD = double.parse(value);
                        },
                        initialValue: currencies.jOD.toString(),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة الدينار الاردني'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الدينار الاردني',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          currencies.tND = double.parse(value);
                        },
                        initialValue: currencies.tND.toString(),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة الدينار التونسي'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الدينار التونسي',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          currencies.eGP = double.parse(value);
                        },
                        initialValue: currencies.eGP.toString(),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة الجنيه المصري'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'الجنيه المصري',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          currencies.cheak = double.parse(value);
                        },
                        initialValue: currencies.cheak.toString(),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة الصك'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'شيك',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          currencies.familyCard = double.parse(value);
                        },
                        initialValue: currencies.familyCard.toString(),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة ارباب الاسر'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'ارباب الاسر',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          currencies.tRYTransfer = double.parse(value);
                        },
                        initialValue: currencies.tRYTransfer.toString(),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة حوالة التركية'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'حوالة التركية',
                        ),
                      ),
                      SizedBox(height: 12),
                      TextFormField(
                        onChanged: (value) {
                          currencies.aEDTransfer = double.parse(value);
                        },
                        initialValue: currencies.aEDTransfer.toString(),
                        style: textStyle(fontSize: 16),
                        validator: RequiredValidator(
                            errorText: 'يرجاء ادخال قيمة حوالة دبي'),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: mainColor)),
                          labelText: 'حوالة دبي',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await setCurrency(this.currencies);
          SweetAlert.show(context,
              confirmButtonText: "تم",
              title: "تم الحفظ بنجاح",
              style: SweetAlertStyle.success);
          // Navigator.pop(context);
        },
        tooltip: "حفظ",
        child: Icon(Icons.save),
      ),
    );
  }

  Future<void> getCurrencies() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getDouble('EUR') == null) {
      await setCurrency(Currency(
        eUR: 1,
        gBP: 1,
        tRY: 1,
        jOD: 1,
        tND: 1,
        eGP: 1,
        cheak: 1,
        familyCard: 1,
        tRYTransfer: 1,
        aEDTransfer: 1,
      ));
    }
    double eur = prefs.getDouble("EUR");
    double gbp = prefs.getDouble("GBP");
    double tyr = prefs.getDouble("TYR");
    double jod = prefs.getDouble("JOD");
    double tnd = prefs.getDouble("TND");
    double egp = prefs.getDouble("EGP");
    double tyrt = prefs.getDouble("TYRT");
    double aedt = prefs.getDouble("AEDT");
    double cheak = prefs.getDouble("CHK");
    double fcard = prefs.getDouble("FCD");

    this.currencies = Currency(
      eUR: eur,
      gBP: gbp,
      tRY: tyr,
      jOD: jod,
      tND: tnd,
      eGP: egp,
      cheak: cheak,
      familyCard: fcard,
      tRYTransfer: tyrt,
      aEDTransfer: aedt,
    );
    setState(() {
      loaded = true;
    });
  }
}
