import 'package:sweetalert/sweetalert.dart';
import 'package:tasami_dashboard_app/data/currency_repository.dart';
import 'package:tasami_dashboard_app/data/model/Vegetables.dart';
import 'package:tasami_dashboard_app/widgets/SharedWidget.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class VegetableScreen extends StatefulWidget {
  @override
  _VegetableScreenState createState() => _VegetableScreenState();
}

class _VegetableScreenState extends State<VegetableScreen> {

  final _formKey = GlobalKey<FormState>();
  ApiCurrencyRepository repository;
  Vegetables vegetables;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    repository = ApiCurrencyRepository();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('الخضروات', style:  textStyle(color: Colors.white),),),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                      onChanged: (value){
                        vegetables.v1 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة الطماطم'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'طماطم',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v2 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة البطاطا'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'بطاطا',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v3 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة الخيار'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'خيار',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v4 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة فلفل حار'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'فلفل حار',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v5 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة فلفل حلو'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'فلفل حلو',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v6 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة بصل'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'بصل',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v7 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة بصل اخضر'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'بصل اخضر',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v8 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة كوسه'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'كوسه',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v9 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة باذنجان'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'باذنجان',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v10 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة جزر'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'جزر',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v11 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة ليمون'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'ليمون',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v12 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة خس'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'خس',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v13 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة ثوم'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'ثوم',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v14 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة شبت'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'شبت',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v15 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة كسبر'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'كسبر',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v16 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة سلك'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'سلك',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v17 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة حبق'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'حبق',
                      ),
                    ),
                    SizedBox(height: 12),
                    TextFormField(
                      onChanged: (value){
                        vegetables.v18 = double.parse(value);
                      },
                      style: textStyle(fontSize: 16),
                      validator: RequiredValidator(errorText: 'يرجاء ادخال قيمة معدنوس'),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                        labelText: 'معدنوس',
                      ),
                    ),
                  ]
              ),
            )
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if(_formKey.currentState.validate()){
            SweetAlert.show(
              context,
              title: "هل انت متاكد",
              style: SweetAlertStyle.confirm,
              showCancelButton: true,
              confirmButtonText: "تأكيد",
              cancelButtonText: "إالغاء",
              onPress: (bool isConfirm) {
                if (isConfirm) {
                  // Todo: Save Data
                  print('confirmed');
                  List<double> price = List<double>();
                  // price.add(double.parse(widget.details.usd.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.eUR.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.gBP.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.tRY.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.jOD.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.tND.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.eGP.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.tRYTransfer.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.aEDTransfer.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.cheak.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.fcard.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.gold.toStringAsFixed(2)));
                  // price.add(double.parse(widget.details.silver.toStringAsFixed(2)));

                  cheack(context , price);
                }
              },
            );
          }
        },
        child: Icon(Icons.save),
      ),
    );
  }
  void cheack(context , price) async{
    bool TT = await repository.addCurrency(price);
    if(TT){
      SweetAlert.show(context,
          confirmButtonText: "تم",
          title: "تم الحفظ بنجاح",
          style: SweetAlertStyle.success);
    }else {
      SweetAlert.show(context,
          confirmButtonText: "تم",
          title: "فشل اتمام العملية",
          style: SweetAlertStyle.error);
    }
  }
}
